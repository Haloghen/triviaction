import numpy as np
from picamera import PiCamera
import time
from tflite_runtime import interpreter as tflite
from PIL import Image
import pygame
import random
import os

morra = ["carta", "sasso", "forbice"]
opzioni = {"sasso": "carta", "carta": "forbice", "forbice": "sasso"}
immagini = {
    "morra": {
        "sasso": "PUGNO.jpg", "carta": "CARTA.jpg", "forbice": "FORBICE.jpg", "vittoria": "VITTORIA.jpg",
        "pareggio": "PAREGGIO.jpg"
    },
    "main": {
        "welcome": "WELCOME.jpg", "gameover": "GAMEOVER.jpg", "vai": "VAI.jpg", 3: "3.jpg", 2: "2.jpg", 1: "1.jpg",
        "lancialo": "LANCIALO.jpg", "trivia": "TRIVIA.jpg", "morra": "MORRA.jpg"
    },
    "trivia": {
        "picasso": "domande/PICASSO_VERO.jpg", "gogh": "domande/GOGH_VERO.jpg", "guernica": "domande/GUERNICA_4.jpg",
        "frozen": "domande/FROZEN_VERO.jpg", "passare": "domande/PASSARE_2.jpg", "wonder": "domande/WONDER_FALSO.jpg",
        "alberi": "domande/ALBERI_VERO.jpg", "newton": "domande/NEWTON_VERO.jpg",
        "operazione": "domande/OPERAZIONE_FALSO.jpg",
        "aladdin": "domande/ALADDIN_4.jpg", "despacito": "domande/DESPACITO_3.jpg", "ebbasta": "domande/EBBASTA_1.jpg",
        "giusto": "GIUSTO.jpg", "sbagliato": "SBAGLIATO.jpg"
    }
            }


trivia = {
    "picasso": True, "gogh": True, "guernica": 4, "frozen": True, "passare": 2, "wonder": False,
    "alberi": True, "newton": True, "operazione": False, "aladdin": 4, "despacito": 3, "ebbasta": 1
}


class Camera:
    c = None

    def __init__(self):
        self.c = PiCamera()
        self.c.resolution = (224, 224)
        time.sleep(2)
        self.c.shutter_speed = self.c.exposure_speed
        self.c.exposure_mode = 'off'

    def capture(self):
        path = "/tmp/prediction.jpg"
        self.c.capture(path)
        return Image.open(path)

    def preview(self, state: bool = False):
        if state:
            self.c.start_preview()
            self.c.preview.fullscreen = False
            self.c.preview.window = (0, 0, 300, 300)
        else:
            self.c.stop_preview()


camera = Camera()


def predict(model: str = "play_stop"):
    path = f"models/{model}"
    data = np.ndarray(shape=(1, 224, 224, 3), dtype=np.float32)
    interpreter = tflite.Interpreter(f"{path}/model_unquant.tflite")
    sign = interpreter.get_signature_runner('serving_default')
    img = camera.capture()
    image_array = np.asarray(img)
    normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1
    data[0] = normalized_image_array

    sequential_raw = interpreter.get_input_details()[0]['name'].split('_')
    seq_in = {
        f"{sequential_raw[2]}_{sequential_raw[3]}_{sequential_raw[4][:-2]}": data
    }

    prediction = sign(**seq_in)

    key = None
    for i in list(prediction.keys()):
        if 'sequential' in i:
            key = i
            break
    max_prediction = max(prediction[key][0])
    label_index = list(prediction[key][0]).index(max_prediction)
    with open(f'{path}/labels.txt', 'r') as labels:
        all_labels = labels.read().split('\n')
        label = all_labels[label_index]
    return label[2:]


class Window:
    w = None

    def __init__(self):
        pygame.init()
        self.w = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

    def immagine(self, gioco: str, stato):
        img = pygame.image.load(f"img/{immagini[gioco][stato]}")
        # img = pygame.transform.scale(img, (1280, 720))
        self.w.blit(img, (0, 0))
        pygame.display.flip()

    def singola_immagine(self, path: str):
        img = pygame.image.load(f"{path}")
        # img = pygame.transform.scale(img, (1280, 720))
        self.w.blit(img, (0, 0))
        pygame.display.flip()


finestra = Window()


def morra_cinese():
    vinto = False

    p_computer = 0
    p_umano = 0
    p_totale = 0
    time.sleep(3)

    while not vinto:
        computer = random.choice(morra)
        umano = "vuoto"
        for i in range(3, 0, -1):

            finestra.immagine("main", i)
            time.sleep(1)
        finestra.immagine("main", "vai")
        while umano == "vuoto":
            umano = predict("morra")

        finestra.immagine("morra", computer)

        time.sleep(2)
        if computer == umano:
            finestra.immagine("morra", "pareggio")
            time.sleep(2)
        else:
            if computer == opzioni[umano]:
                p_computer += 1
                p_totale += 1
            else:
                p_umano += 1
                p_totale += 1

        if p_umano == 2:
            finestra.immagine("morra", "vittoria")
            vinto = True
            time.sleep(2)
        elif p_computer == 2:
            finestra.immagine("main", "gameover")
            vinto = True
            time.sleep(2)
        finestra.singola_immagine(f"img/punteggi/punt_{p_umano}_{p_computer}.jpg")
        time.sleep(2)


def trivia_game():
    num_domande = 0
    time.sleep(3)
    while num_domande < 4:
        domanda, risposta = random.choice(list(trivia.items()))
        finestra.immagine("trivia", domanda)
        r_utente = "vuoto"
        time.sleep(1)
        camera.preview(True)
        while r_utente == "vuoto":
            r_utente = predict("vero_falso" if type(risposta) == bool else "numeri")
        camera.preview(False)

        possibili_risposte = {
            "uno": 1, "due": 2, "tre": 3, "quattro": 4, "vero": True, "falso": False
        }

        r_utente = possibili_risposte[r_utente]
        if r_utente == risposta:
            finestra.immagine("trivia", "giusto")
        else:
            finestra.immagine("trivia", "sbagliato")

        num_domande += 1
        time.sleep(3)


def start():
    finestra.immagine("main", "welcome")
    time.sleep(2)
    camera.preview(True)
    while True:
        prediction = predict()
        if prediction == "play":
            camera.preview(False)
            finestra.immagine("main", "lancialo")
            time.sleep(2)
            camera.preview(True)
            while True:
                prediction = predict("selezione")
                if prediction != "vuoto":
                    camera.preview(False)
                    if prediction == "morra":
                        finestra.immagine("main", "morra")
                        morra_cinese()
                        finestra.immagine("main", "welcome")
                        camera.preview(True)
                        break
                    elif prediction == "trivia":
                        finestra.immagine("main", "trivia")
                        trivia_game()
                        finestra.immagine("main", "welcome")
                        camera.preview(True)
                        break
                    else:
                        finestra.immagine("main", "welcome")
                        camera.preview(True)
                        break

start()
